# sungazer/celery-bundle

Bundle to implement Celery protocol (both worker and client) for Symfony and PHP

Currently supported brokers: 

* [x] AMQP

Currently supported result backend:

* [x] RPC (via AMQP direct reply-to)

## Usage

TODO
