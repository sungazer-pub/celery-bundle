<?php

namespace Sungazer\Bundle\SungazerCeleryBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/*
 * Bundle description
 */
class SungazerCeleryBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
    }
}

