<?php


namespace Sungazer\Bundle\SungazerCeleryBundle\Doctrine;


use Doctrine\Persistence\ManagerRegistry;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\Event\TaskFailedCeleryEvent;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\Event\TaskReceivedCeleryEvent;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\Event\TaskSucceededCeleryEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class DoctrineTransactionListener extends AbstractDoctrineListener implements EventSubscriberInterface
{

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            TaskReceivedCeleryEvent::NAME  => ['onTaskReceived', 9999], // First one being called
            TaskSucceededCeleryEvent::NAME => ['onTaskSucceeded', -9999], // Last one being called
            TaskFailedCeleryEvent::NAME    => ['onTaskFailed', -9999], // Last one being called
        ];
    }

    public function onTaskReceived(TaskReceivedCeleryEvent $event)
    {
        if ($em = $this->getEntityManager()) {
            if($this->logger){
                $this->logger->debug('Beginning transaction');
            }
            $em->getConnection()->beginTransaction();
        }
    }

    public function onTaskFailed(TaskFailedCeleryEvent $event)
    {
        if ($em = $this->getEntityManager()) {
            if($this->logger){
                $this->logger->debug('Rolling back transaction');
            }
            $em->getConnection()->rollBack();
        }
    }

    public function onTaskSucceeded(TaskSucceededCeleryEvent $event)
    {
        if ($em = $this->getEntityManager()) {
            if($this->logger){
                $this->logger->debug('Committing transaction');
            }
            $em->getConnection()->commit();
        }
    }

}