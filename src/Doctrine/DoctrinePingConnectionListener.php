<?php


namespace Sungazer\Bundle\SungazerCeleryBundle\Doctrine;


use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\Persistence\ManagerRegistry;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\Event\TaskReceivedCeleryEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class DoctrinePingConnectionListener extends AbstractDoctrineListener implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return [
            TaskReceivedCeleryEvent::NAME => 'onTaskReceived'
        ];
    }

    public function onTaskReceived(TaskReceivedCeleryEvent $event)
    {
        $em = $this->getEntityManager();
        if($em instanceof EntityManagerInterface) {
            if($this->logger){
                $this->logger->debug('Pinging doctrine connection');
            }
            $this->pingConnection($em);
        }
    }

    private function pingConnection(EntityManagerInterface $entityManager)
    {
        $connection = $entityManager->getConnection();

        try {
            $connection->executeQuery($connection->getDatabasePlatform()->getDummySelectSQL());
        } catch (Exception $e) {
            $connection->close();
            $connection->connect();
        }

        if (!$entityManager->isOpen()) {
            $this->managerRegistry->resetManager($this->entityManagerName);
        }
    }
}