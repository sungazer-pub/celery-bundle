<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Sungazer\Bundle\SungazerCeleryBundle\Doctrine;



use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;

abstract class AbstractDoctrineListener
{
    protected $managerRegistry;
    protected $entityManagerName;
    /**
     * @var LoggerInterface|null
     */
    protected $logger;

    public function __construct(ManagerRegistry $managerRegistry, string $entityManagerName = null, ?LoggerInterface $logger = null)
    {
        $this->managerRegistry   = $managerRegistry;
        $this->entityManagerName = $entityManagerName;
        $this->logger = $logger;
    }

    public function getEntityManager(): EntityManagerInterface
    {
        return $this->managerRegistry->getManager($this->entityManagerName);
    }
}
