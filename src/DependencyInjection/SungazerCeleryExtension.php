<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 20/02/19
 * Time: 9.14
 */

namespace Sungazer\Bundle\SungazerCeleryBundle\DependencyInjection;


use Sungazer\Bundle\SungazerCeleryBundle\Doctrine\DoctrinePingConnectionListener;
use Sungazer\Bundle\SungazerCeleryBundle\Doctrine\DoctrineTransactionListener;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Alias;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;

class SungazerCeleryExtension extends Extension
{

    /**
     * Loads a specific configuration.
     *
     * @throws \InvalidArgumentException When provided tag is not defined in this extension
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();

        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('sungazer_celery.broker_dsn', $config['broker_dsn']);
        $container->setParameter('sungazer_celery.backend_dsn', $config['backend_dsn']);
        $container->setParameter('sungazer_celery.worker_name', $config['worker_name']);
        $container->setParameter('sungazer_celery.client_name', $config['client_name']);
        $container->setParameter('env(CELERY_WORKER_NAME)',null);
        $container->setParameter('env(CELERY_CLIENT_NAME)',null);

        $container->registerForAutoconfiguration(CeleryTaskHandlerInterface::class)
            ->addTag('sungazer_celery.task_handler')
            ;

        $loader = new XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));

        $loader->load('commands.xml');
        $loader->load('services.xml');

        if($config['default_listeners']['doctrine_ping']){
            $container->register('sungazer_celery.event_subscribers.doctrine_ping',DoctrinePingConnectionListener::class)
                ->setAutoconfigured(true)
                ->setAutowired(true)
                ->addTag('monolog.logger',['channel' => 'celery'])
                ;
        }

        if($config['default_listeners']['doctrine_transaction']){
            $container->register('sungazer_celery.event_subscribers.doctrine_transaction',DoctrineTransactionListener::class)
                ->setAutoconfigured(true)
                ->setAutowired(true)
                ->addTag('monolog.logger',['channel' => 'celery'])
            ;
        }

    }
}