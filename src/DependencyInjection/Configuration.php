<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 20/02/19
 * Time: 11.49
 */

namespace Sungazer\Bundle\SungazerCeleryBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

// https://symfony.com/doc/current/bundles/configuration.html

class Configuration implements ConfigurationInterface
{

    /**
     * Generates the configuration tree builder.
     *
     * @return TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('sungazer_celery');

        $rootNode = $treeBuilder->getRootNode();

        $rootNode->children()
            ->scalarNode('broker_dsn')
            ->defaultValue('amqp://localhost/')
            ->end()
        ;

        $rootNode->children()
            ->scalarNode('backend_dsn')
            ->defaultValue('rpc://')
            ->end()
        ;

        $rootNode->children()
            ->scalarNode('worker_name')
            ->defaultValue('%env(CELERY_WORKER_NAME)%')
            ->end()
        ;

        $rootNode->children()
            ->scalarNode('client_name')
            ->defaultValue('%env(CELERY_CLIENT_NAME)%')
            ->end()
        ;

        $rootNode
            ->children()
                ->arrayNode('default_listeners')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->booleanNode('doctrine_ping')->defaultValue(false)->end()
                        ->booleanNode('doctrine_transaction')->defaultValue(false)->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
