<?php


namespace Sungazer\Bundle\SungazerCeleryBundle\DependencyInjection;


interface CeleryTaskHandlerInterface
{
    /**
     * @return string
     */
    public function getTask();

    /**
     * @return string
     */
    public function getQueue();

    public function __invoke($args, $kwargs);

}