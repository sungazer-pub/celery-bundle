<?php


namespace Sungazer\Bundle\SungazerCeleryBundle\Command;


use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Brokers\AmqpBroker;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\CeleryClient;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\CeleryClientInterface;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\CeleryWorker;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CeleryDispatchTaskCommand extends Command
{
    protected static $defaultName = 'celery:dispatch-task';
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var CeleryClientInterface
     */
    private $celeryClient;

    public function __construct(LoggerInterface $logger, ContainerInterface $container, CeleryClientInterface $celeryClient, string $name = null)
    {
        parent::__construct($name);
        $this->logger = $logger;
        $this->container = $container;
        $this->celeryClient = $celeryClient;
    }

    protected function configure()
    {
        $this
            ->addOption('name', null, InputOption::VALUE_REQUIRED)
            ->addOption('queue', null, InputOption::VALUE_REQUIRED)
            ->addOption('task', null, InputOption::VALUE_REQUIRED, 'Task name')
            ->addOption('body', null, InputOption::VALUE_REQUIRED, 'Task body (JSON encoded)')
            ->setDescription('Manually dispatches a task');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $taskName = $input->getOption('task');
        $taskBody = $input->getOption('body') ?? '{}';
        $queue    = $input->getOption('queue');


        if($name = $input->getOption('name')){
            $this->celeryClient->setName($name);
        }

        $res = $this->celeryClient->dispatchTask($taskName, $queue, json_decode($taskBody, true));

        $res->wait(5);
        dump([
            'status' => $res->getResultStatus(),
            'result' => $res->getResultBody(),
        ]);

        return 0;
    }

}