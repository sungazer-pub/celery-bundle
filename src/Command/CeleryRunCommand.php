<?php


namespace Sungazer\Bundle\SungazerCeleryBundle\Command;


use Psr\Log\LoggerInterface;
use Sungazer\Bundle\SungazerCeleryBundle\DependencyInjection\CeleryTaskHandlerInterface;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\CeleryWorkerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CeleryRunCommand extends Command
{
    protected static $defaultName = 'celery:run';
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var CeleryTaskHandlerInterface[] | array
     */
    private $handlers;
    /**
     * @var CeleryWorkerInterface
     */
    private $worker;

    public function __construct(LoggerInterface $logger, ContainerInterface $container, iterable $handlers, CeleryWorkerInterface $worker, string $name = null)
    {
        parent::__construct($name);
        $this->logger    = $logger;
        $this->container = $container;
        $this->handlers  = $handlers;
        $this->worker    = $worker;
    }

    protected function configure()
    {
        $this
            ->addOption('name', null, InputOption::VALUE_REQUIRED, 'Worker name, random UUID if not set')
            ->setDescription('Starts a Celery worker in the foreground');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        if ($name = $input->getOption('name')) {
            $this->worker->setName($name);
        }

        foreach ($this->handlers as $handler) {
            $this->worker->registerTask($handler->getTask(), $handler->getQueue(), $handler);
        }

        $this->worker->connectAndStart();


        return 0;
    }

}