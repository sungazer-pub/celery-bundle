<?php


namespace Sungazer\Bundle\SungazerCeleryBundle\Lib\Brokers;


use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\CeleryMessage;

interface BrokerInterface
{
    public function connect();
    public function disconnect();

    public function publish(CeleryMessage $msg, string $exchange, ?string $routingKey = null);

    public function addQueue(string $name);

}