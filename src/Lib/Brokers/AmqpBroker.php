<?php


namespace Sungazer\Bundle\SungazerCeleryBundle\Lib\Brokers;


use AMQPChannel;
use AMQPConnection;
use AMQPEnvelope;
use AMQPException;
use AMQPExchange;
use AMQPQueue;
use AMQPQueueException;
use InvalidArgumentException;
use Nyholm\Dsn\Configuration\Dsn;
use Nyholm\Dsn\Configuration\Url;
use Nyholm\Dsn\DsnParser;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use RuntimeException;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\CeleryMessage;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\CeleryTaskMessage;

class AmqpBroker implements BrokerInterface
{
    use LoggableBrokerTrait;

    /**
     * @var AMQPConnection
     */
    private $conn;

    /**
     * @return AMQPConnection
     */
    public function getConn(): ?AMQPConnection
    {
        return $this->conn;
    }

    /**
     * @param AMQPConnection $conn
     */
    public function setConn(AMQPConnection $conn): void
    {
        $this->conn = $conn;
    }

    /**
     * @return AMQPChannel
     */
    public function getChannel(): AMQPChannel
    {
        return $this->channel;
    }

    /**
     * @param AMQPChannel $channel
     */
    public function setChannel(AMQPChannel $channel): void
    {
        $this->channel = $channel;
    }
    /**
     * @var AMQPChannel
     */
    private $channel;
    /**
     * @var Url
     */
    private $dsn;

    /**
     * @var array
     */
    private $queues = [];

    /**
     * AmqpBroker constructor.
     * @param string | Url $dsn
     * @param string $workerName
     * @param LoggerInterface|null $logger
     */
    public function __construct(Url $dsn, ?LoggerInterface $logger = null)
    {
        $parsed = is_string($dsn) ? DsnParser::parse($dsn) : $dsn;
        if ($parsed->getScheme() !== 'amqp') {
            throw new InvalidArgumentException('Only AMQP scheme is supported for this broker');
        }
        $this->dsn  = $parsed;
        if($this->dsn->getPort() === null){
            $this->dsn = $this->dsn->withPort(5672);
        }
        if ($logger) {
            $this->setLogger($logger);
        }
    }

    public function getDsn(){
        return $this->dsn;
    }

    public function connect()
    {
        $this->conn = new AMQPConnection([
            'host'     => $this->dsn->getHost(),
            'port'     => $this->dsn->getPort() ?? 5672,
            'vhost'    => ltrim($this->dsn->getPath(), "/"),
            'login'    => $this->dsn->getUser(),
            'password' => $this->dsn->getPassword()
        ]);
        $this->conn->connect();
        $this->channel = new AMQPChannel($this->conn);
        $this->channel->setPrefetchCount(1);
        $this->channel->setGlobalPrefetchCount(1);
//        $this->assertExchange('', 'direct', AMQP_DURABLE);
    }

    public function disconnect(){
        $this->channel->close();
        $this->conn->disconnect();

        $this->channel = null;
        $this->conn = null;
    }

    public function processMessages(callable $onMessage = null, ?callable $onTimeout = null){
        $consumerTag = Uuid::uuid4()->toString();
        // Register to queues
        foreach ($this->queues as $name => $val){
            if($val === null){
                $this->logger->info("[AMQPBroker] Creating queue $name");
                $q = $this->assertQueue($name, AMQP_DURABLE + AMQP_AUTODELETE);
//                $q->bind('default',$name);
                $q->consume(null,AMQP_NOPARAM, $consumerTag);
                $this->queues[$name] = $q;
            }
        }
        $this->logger->info("[AMQPBroker] Starting to process messages...");
        $this->conn->setReadTimeout(30);


        while(true) {
            try {
                $q = reset($this->queues);
                $q->consume(function (AMQPEnvelope $envelope, AMQPQueue $queue) use ($onMessage) {

                    if($envelope->getContentType() !== 'application/json'){
                        $this->logger->error("Unsupported content type received: " . $envelope->getContentType());
                        $queue->reject($envelope->getDeliveryTag(), AMQP_REQUEUE);
                        return true;
                    }

                    if($envelope->getContentEncoding() !== 'utf-8'){
                        $this->logger->error("Unsupported content encoding received: " . $envelope->getContentEncoding());
                        $queue->reject($envelope->getDeliveryTag(), AMQP_REQUEUE);
                        return true;
                    }

                    $msg = new CeleryTaskMessage();
                    $msg->setHeaders($envelope->getHeaders())
                        ->updateProperties([
                            CeleryTaskMessage::PROP_CONTENT_TYPE => $envelope->getContentType(),
                            CeleryTaskMessage::PROP_CONTENT_ENCODING => $envelope->getContentEncoding(),
                            CeleryTaskMessage::PROP_CORRELATION_ID => $envelope->getCorrelationId(),
                            CeleryTaskMessage::PROP_REPLY_TO => $envelope->getReplyTo()
                        ])
                        ->setBody(json_decode($envelope->getBody(),true))
                    ;

                    // Call handler
                    $res = $onMessage($msg);

                    if($res) {
                        $queue->ack($envelope->getDeliveryTag());
                    } else {
                        $queue->reject($envelope->getDeliveryTag());
                    }
                    return true;
                },AMQP_JUST_CONSUME,  $consumerTag);
//                $nQueues = count($this->queues);
//                $i = 0;
//                foreach ($this->queues as $name => $q){
//                    $this->logger->debug("Consuming from queue $name");
//                    if($i === $nQueues - 1){
//                        $q->consume(function (AMQPEnvelope $envelope, AMQPQueue $queue) use ($onMessage) {
//
//                            if($envelope->getContentType() !== 'application/json'){
//                                $this->logger->error("Unsupported content type received: " . $envelope->getContentType());
//                                $queue->reject($envelope->getDeliveryTag(), AMQP_REQUEUE);
//                                return false;
//                            }
//
//                            if($envelope->getContentEncoding() !== 'utf-8'){
//                                $this->logger->error("Unsupported content encoding received: " . $envelope->getContentEncoding());
//                                $queue->reject($envelope->getDeliveryTag(), AMQP_REQUEUE);
//                                return false;
//                            }
//
//                            $msg = new CeleryTaskMessage();
//                            $msg->setHeaders($envelope->getHeaders())
//                                ->updateProperties([
//                                    CeleryTaskMessage::PROP_CONTENT_TYPE => $envelope->getContentType(),
//                                    CeleryTaskMessage::PROP_CONTENT_ENCODING => $envelope->getContentEncoding(),
//                                    CeleryTaskMessage::PROP_CORRELATION_ID => $envelope->getCorrelationId(),
//                                    CeleryTaskMessage::PROP_REPLY_TO => $envelope->getReplyTo()
//                                ])
//                                ->setBody(json_decode($envelope->getBody(),true))
//                            ;
//
//
//                            $onMessage($msg);
//                            $queue->ack($envelope->getDeliveryTag());
//                            return false;
//                        },AMQP_NOPARAM);
//                    } else {
//                        $q->consume();
//                    }
//                    $i += 1;
//                }
            } catch (AMQPException $ex) {
                if ($ex->getMessage() === 'Consumer timeout exceed') {
                    // TODO Need a way to clear up consumers
//                    $this->taskQueue->cancel();
                    if($onTimeout){
                        $onTimeout();
                    }
                    continue;
                }
                throw $ex;
            }
        }
    }

    public function addQueue(string $name){
        if(!array_key_exists($name,$this->queues)){
            $this->queues[$name] = null;
        }
    }


    private function assertExchange(string $name, string $type, int $flags)
    {
        $ex = new AMQPExchange($this->channel);

        $ex->setName($name);
        $ex->setType($type);
        $ex->setFlags($flags);
        $ex->declareExchange();
        return $ex;
    }

    private function assertQueue(string $name, int $flags)
    {
        $q = new AMQPQueue($this->channel);
        $q->setName($name);
        $q->setFlags($flags);
        $q->declareQueue();
        return $q;
    }

    public function publish(CeleryMessage $msg, string $exchange, ?string $routingKey = null)
    {
        $ex = (new AMQPExchange($this->channel));
        $ex->setName($exchange);

        if (!$ex->publish(
            json_encode($msg->getBody()),
            $routingKey,
            AMQP_NOPARAM,
            array_merge($msg->getProperties(), [
                'headers'          => $msg->getHeaders(),
                'content_type'     => 'application/json',
                'content_encoding' => 'utf-8',
            ])
        )
        ) {
            if ($this->logger) {
                $this->logger->error("Error publishing message");
            }
            // TODO throw exception
        }
        if ($this->logger) {
            $this->logger->debug('Dispatched message', ['exchange' => $exchange, 'routingKey' => $routingKey]);
        }

    }
}