<?php


namespace Sungazer\Bundle\SungazerCeleryBundle\Lib\Brokers;


use Psr\Log\LoggerInterface;

trait LoggableBrokerTrait
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function setLogger(LoggerInterface $logger){
        $this->logger = $logger;
    }


}