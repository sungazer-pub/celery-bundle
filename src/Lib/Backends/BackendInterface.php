<?php


namespace Sungazer\Bundle\SungazerCeleryBundle\Lib\Backends;


interface BackendInterface
{
    public function connect();

    public function disconnect();

    public function getTaskResult(string $expectedTaskId, int $maxWait = -1);

}