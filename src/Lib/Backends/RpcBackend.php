<?php


namespace Sungazer\Bundle\SungazerCeleryBundle\Lib\Backends;


use AMQPChannel;
use AMQPConnection;
use AMQPEnvelope;
use AMQPException;
use AMQPQueue;
use Ramsey\Uuid\Uuid;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\Exception\AsyncResultTimeoutException;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Brokers\AmqpBroker;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\CeleryMessage;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\CeleryTaskMessage;

class RpcBackend implements BackendInterface
{

    /**
     * @var AmqpBroker
     */
    private $broker;

    /**
     * @var AMQPChannel
     */
    private $channel;
    /**
     * @var AMQPQueue
     */
    private $replyToQueue;
    /**
     * @var string
     */
    private $consumerTag;

    /**
     * @var array
     */
    private $repliesCache = [];

    public function __construct(AmqpBroker $broker)
    {
        $this->broker      = $broker;
        $this->consumerTag = Uuid::uuid4()->toString();
    }

    public function connect()
    {
        $this->channel = $this->broker->getChannel();
        $this->broker->getConn()->setReadTimeout(30);

        // Dummy consume to signal RabbitMQ that we want to listen to replies
        // See https://github.com/php-amqp/php-amqp/issues/155 and https://github.com/gjedeer/celery-php/blob/master/src/CeleryAbstract.php
        $this->replyToQueue = new AMQPQueue($this->channel);
        $this->replyToQueue->setName('amq.rabbitmq.reply-to');
        $this->replyToQueue->consume(null, AMQP_AUTOACK, $this->consumerTag);
    }

    public function disconnect()
    {
        $this->replyToQueue->cancel();
//        $this->channel->close();
        $this->channel = null;
    }

    public function storeTaskResult(CeleryTaskMessage $task, string $state, $result, $traceback = null){
        $replyTo = $task->getProperty(CeleryTaskMessage::PROP_REPLY_TO);
        if(!$replyTo){
            throw new \InvalidArgumentException('Cannot reply without a reply_to property');
        }
        $msg = new CeleryMessage([
            'status' => $state,
            'result' => $result,
            'traceback' => $traceback,
            'task_id' => $task->getHeader(CeleryTaskMessage::HEADER_ID),
            'children' => []
        ]);
        $this->broker->publish($msg,'',$replyTo);
    }

    public function clearReplyCache(){
        $this->repliesCache = [];
    }

    public function getTaskResult(string $expectedTaskId, int $maxWait = -1)
    {
        if (!array_key_exists($expectedTaskId, $this->repliesCache)) {
            if($maxWait > 0){
                $this->broker->getConn()->setReadTimeout($maxWait);
            }
            $waitingStart = microtime(true);
            while (true) {
                try {
                    $this->replyToQueue->consume(
                        function (AMQPEnvelope $envelope, AMQPQueue $queue) use ($expectedTaskId) {
                            $body   = json_decode($envelope->getBody(), true);
                            $taskId = $body['task_id'] ?? null;
                            if (!$taskId) {
                                return true;
                            }
                            $this->repliesCache[$taskId] = ['ts' => microtime(true), 'body' => $body];
                            if ($expectedTaskId === $taskId) {
                                return false;
                            }
                            return true;
                        }, AMQP_JUST_CONSUME, $this->consumerTag
                    );
                    // We reach here only if we do not terminate due to a timeout
                    break;
                } catch (AMQPException $ex) {
                    if ($ex->getMessage() === 'Consumer timeout exceed') {
                        if((microtime(true) - $waitingStart) > ($maxWait - 0.1)){
                            throw new AsyncResultTimeoutException();
                        }
                        continue;
                    }
                    throw $ex;
                }
            }
        }
        // Here we should have the result
        return $this->repliesCache[$expectedTaskId]['body'] ?? null;
    }

}