<?php


namespace Sungazer\Bundle\SungazerCeleryBundle\Lib\Core;


use Sungazer\Bundle\SungazerCeleryBundle\Lib\Backends\BackendInterface;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\Exception\AsyncResultFailureException;

/**
 * Celery built in states https://docs.celeryproject.org/en/latest/userguide/tasks.html#built-in-states
 */
class AsyncResult
{
    const STATUS_SUCCESS = 'SUCCESS';
    const STATUS_FAILURE = 'FAILURE';

    private $taskId;
    /**
     * @var BackendInterface
     */
    private $backend;

    /**
     * @var array
     */
    private $taskResult = null;

    public function __construct(string $taskId, BackendInterface $backend)
    {
        $this->taskId = $taskId;
        $this->backend = $backend;
    }

    public function wait(int $maxWait = -1, bool $throw = true){
        if(!$this->taskResult){
            $res = $this->backend->getTaskResult($this->taskId, $maxWait);
            $this->taskResult = $res;
            if($throw){
                if($this->taskResult['status'] === self::STATUS_FAILURE){
//                    dump($this->taskResult);
                    $type = $this->taskResult['result']['exc_type'] ?? '';
                    $message = $this->taskResult['result']['exc_message'] ?? '';
                    if(is_array($message)){
                        $message = implode('\n',$message);
                    }
                    $msg = sprintf("%s: %s",$type,$message);
                    throw new AsyncResultFailureException($msg);
                }
            }
        }
    }

    public function getResultBody(bool $throw = true){
        $this->wait(30, $throw);
        return $this->taskResult['result'] ?? null;
    }

    public function getResultTraceback(bool $throw = true){
        $this->wait(30, $throw);
        return $this->taskResult['traceback'] ?? null;
    }

    public function getResultStatus(){
        $this->wait(30, false);
        return $this->taskResult['status'] ?? null;
    }

}