<?php


namespace Sungazer\Bundle\SungazerCeleryBundle\Lib\Core;


interface CeleryWorkerInterface
{
    public function connectAndStart();
    public function stopAndDisconnect();
    public function registerTask(string $task, string $queue, $fn);
    public function setName(string $name);
}