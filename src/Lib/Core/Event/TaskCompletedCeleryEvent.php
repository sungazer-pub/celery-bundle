<?php


namespace Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\Event;


use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\CeleryTaskMessage;
use Symfony\Contracts\EventDispatcher\Event;

class TaskCompletedCeleryEvent extends CeleryEvent
{
    public const NAME = 'sungazer_celery.events.task.completed';
    /**
     * @var CeleryTaskMessage
     */
    private $msg;

    public function __construct(CeleryTaskMessage $msg)
    {
        $this->msg = $msg;
    }

    /**
     * @return CeleryTaskMessage
     */
    public function getMsg(): CeleryTaskMessage
    {
        return $this->msg;
    }

    /**
     * @param CeleryTaskMessage $msg
     * @return TaskReceivedCeleryEvent
     */
    public function setMsg(CeleryTaskMessage $msg): TaskReceivedCeleryEvent
    {
        $this->msg = $msg;
        return $this;
    }

}