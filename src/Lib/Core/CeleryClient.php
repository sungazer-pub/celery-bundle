<?php


namespace Sungazer\Bundle\SungazerCeleryBundle\Lib\Core;


use InvalidArgumentException;
use Nyholm\Dsn\DsnParser;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Backends\BackendInterface;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Backends\RpcBackend;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Brokers\AmqpBroker;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Brokers\BrokerInterface;

class CeleryClient implements CeleryClientInterface
{
    /**
     * @var BrokerInterface
     */
    private $broker;

    /**
     * @var BackendInterface
     */
    private $backend;

    /**
     * Logical clock for the worker
     * @var int
     */
    private $logicalClock = 0;
    /**
     * @var string
     */
    private $name;
    /**
     * @var LoggerInterface|null
     */
    private $logger;
    /**
     * @var bool
     */
    private $connected = false;

    public function __construct(string $brokerDsn, string $backendDsn, ?string $name = null, ?LoggerInterface $logger = null)
    {
        $brokerDsnParsed = DsnParser::parse($brokerDsn);
        $scheme          = $brokerDsnParsed->getScheme();
        if ($scheme === 'amqp') {
            $this->broker = new AmqpBroker($brokerDsnParsed, $logger);
        } else {
            throw new InvalidArgumentException("Invalid broker scheme $scheme. Supported schemes are: amqp");
        }

        $backendDsnParsed = DsnParser::parse($backendDsn);
        $scheme           = $backendDsnParsed->getScheme();
        if ($scheme === 'rpc') {
            if (($this->broker instanceof AmqpBroker)) {
                $this->backend = new RpcBackend($this->broker);
            } else {
                throw new InvalidArgumentException("RPC backend only works with AMQP broker");
            }
        } else {
            throw new InvalidArgumentException("Invalid backend scheme $scheme. Supported schemes are: rpc");
        }

        $this->name   = $name ?? Uuid::uuid4()->toString();
        $this->logger = $logger;
    }

    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function connect()
    {
        $this->broker->connect();
        $this->backend->connect();
        $this->connected = true;
    }

    /**
     * Clears any cache or storage that the storage backend might need.
     * This should be called after each request in case of long lived scripts.
     * WARNING: After calling this, any un-awaited AsyncResult that was created prior to this call will go in timeout!
     */
    public function clear()
    {
        if ($this->backend instanceof RpcBackend) {
            $this->backend->clearReplyCache();
        }
    }

    /**
     * @param string $taskName
     * @param string $queue
     * @param array $body = [
     *     'args' => [],
     *     'kwargs' => [],
     *     'embed' => [],
     * ]
     */
    public function dispatchTask(string $taskName, string $queue, array $body): AsyncResult
    {
        if(!$this->connected){
            $this->connect();
        }
        $correlationId = Uuid::uuid4()->toString();
        $taskId        = Uuid::uuid4()->toString();

        $msg = $this->createTaskMessage($taskName, $taskId, $correlationId, $body, [
        ], [
            CeleryTaskMessage::PROP_REPLY_TO => 'amq.rabbitmq.reply-to'
        ]);
        $this->broker->publish($msg, '', $queue);
        return new AsyncResult($taskId, $this->backend);
    }

    private function createTaskMessage(string $taskName, string $taskId, string $correlationId, array $body = [], array $headers = [], array $properties = [])
    {
        $headers    = array_merge($headers, [
            CeleryTaskMessage::HEADER_LANG   => '*',
            CeleryTaskMessage::HEADER_TASK   => $taskName,
            CeleryTaskMessage::HEADER_ID     => $taskId,
            CeleryTaskMessage::HEADER_ORIGIN => $this->name,
        ]);
        $properties = array_merge($properties, [
            CeleryTaskMessage::PROP_CORRELATION_ID => $correlationId,
        ]);
        $kwargs = $body['kwargs'] ?? [];
        $embed = $body['embed'] ?? [];
        $kwargs = count($kwargs) > 0 ? $kwargs : new \stdClass();
        $embed = count($embed) > 0 ? $embed : new \stdClass();
        return new CeleryTaskMessage([$body['args'] ?? [], $kwargs, $embed], $headers, $properties);
    }

}