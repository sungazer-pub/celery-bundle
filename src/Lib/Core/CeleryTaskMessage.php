<?php


namespace Sungazer\Bundle\SungazerCeleryBundle\Lib\Core;


class CeleryTaskMessage extends CeleryMessage
{
    public const PROP_REPLY_TO       = 'reply_to';
    public const PROP_CORRELATION_ID = 'correlation_id';

    public const HEADER_LANG       = 'lang';
    public const HEADER_TASK       = 'task';
    public const HEADER_ID         = 'id';
    public const HEADER_ORIGIN     = 'origin';
    public const HEADER_ARGSREPR   = 'argsrepr';
    public const HEADER_KWARGSREPR = 'kwargsrepr';

    public function __construct(array $body = [], array $headers = [], array $properties = [])
    {
        parent::__construct($body, $headers, $properties);
    }

    public function getArgs(){
        return $this->body[0] ?? $this->body['args'] ?? null;
    }

    public function getKwargs(){
        return $this->body[1] ?? $this->body['kwargs'] ?? null;
    }

    public function getEmbed(){
        return $this->body[2] ?? $this->body['embed'] ?? null;
    }


}