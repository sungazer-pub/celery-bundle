<?php


namespace Sungazer\Bundle\SungazerCeleryBundle\Lib\Core;


use Exception;
use InvalidArgumentException;
use Nyholm\Dsn\DsnParser;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Backends\BackendInterface;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Backends\RpcBackend;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Brokers\AmqpBroker;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Brokers\BrokerInterface;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\Event\TaskCompletedCeleryEvent;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\Event\TaskFailedCeleryEvent;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\Event\TaskReceivedCeleryEvent;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\Event\TaskSucceededCeleryEvent;
use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\Exception\NoHandlersException;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class CeleryWorker implements CeleryWorkerInterface
{
    /**
     * @var BrokerInterface
     */
    private $broker;

    /**
     * Logical clock for the worker
     * @var int
     */
    private $logicalClock = 0;
    /**
     * @var string
     */
    private $name;

    private $processedTasks = 0;

    private $activeTasks = 0;
    /**
     * @var LoggerInterface|null
     */
    private $logger;

    /**
     * @var array
     */
    private $handlers = [];
    /**
     * @var EventDispatcher|EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var BackendInterface
     */
    private $backend;

    public function __construct(string $brokerDsn, string $backendDsn, ?string $name = null, ?LoggerInterface $logger = null, ?EventDispatcherInterface $eventDispatcher = null)
    {
        $brokerDsnParsed = DsnParser::parse($brokerDsn);
        $scheme          = $brokerDsnParsed->getScheme();
        if ($scheme === 'amqp') {
            $this->broker = new AmqpBroker($brokerDsnParsed, $logger);
        } else {
            throw new InvalidArgumentException("Invalid broker scheme $scheme. Supported schemes are: amqp");
        }

        $backendDsnParsed = DsnParser::parse($backendDsn);
        $scheme           = $backendDsnParsed->getScheme();
        if ($scheme === 'rpc') {
            if (($this->broker instanceof AmqpBroker)) {
                $this->backend = new RpcBackend($this->broker);
            } else {
                throw new InvalidArgumentException("RPC backend only works with AMQP broker");
            }
        } else {
            throw new InvalidArgumentException("Invalid backend scheme $scheme. Supported schemes are: rpc");
        }

        $this->name            = $name ?? Uuid::uuid4()->toString();
        $this->logger          = $logger;
        $this->eventDispatcher = $eventDispatcher ?? new EventDispatcher();
    }

    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function connectAndStart()
    {
        if (count($this->handlers) === 0) {
            throw new NoHandlersException();
        }
        $dsn = $this->broker->getDsn();
        $this->logger->debug(sprintf("Connecting to broker %s://%s:*********@%s:%s%s", $dsn->getScheme(), $dsn->getUser(), $dsn->getHost(), $dsn->getPort(), $dsn->getPath()));
        $this->broker->connect();
        $this->backend->connect();

        $this->broker->publish($this->createEventMessage(['type' => 'worker-online']), 'celeryev');

        // Start consume loop
        $this->broker->processMessages(
            function (CeleryTaskMessage $msg) {
                $this->logger->info("Received message!", ['headers' => $msg->getHeaders(), 'properties' => $msg->getProperties(), 'body' => $msg->getBody()]);
                $task   = $msg->getHeader(CeleryTaskMessage::HEADER_TASK);
                $taskId = $msg->getHeader(CeleryTaskMessage::HEADER_ID);

                $this->sendTaskReceived($task, $taskId, $msg->getArgs(), $msg->getKwargs());
                $this->sendTaskStarted($taskId);

                if (!array_key_exists($task, $this->handlers)) {
                    $this->logger->warning("Received task $task but no handler is configured");
                    return false;
                }

                $this->eventDispatcher->dispatch(new TaskReceivedCeleryEvent($msg), TaskReceivedCeleryEvent::NAME);
                try {
                    $time       = microtime(true);
                    $taskResult = $this->handlers[$task]($msg->getArgs(), $msg->getKwargs());
                    $delta      = microtime(true) - $time;
                    $this->eventDispatcher->dispatch(new TaskSucceededCeleryEvent($msg), TaskSucceededCeleryEvent::NAME);
                    $this->sendTaskSucceeded($taskId, $delta, $taskResult);
                    $this->backend->storeTaskResult($msg, 'ok', $taskResult);
                } catch (Exception $e) {
                    $this->logger->error("Task FAILED: " . $e->getMessage(), ['trace' => $e->getTraceAsString()]);
                    $this->eventDispatcher->dispatch(new TaskFailedCeleryEvent($msg), TaskFailedCeleryEvent::NAME);
                    $this->sendTaskFailed($taskId, $e->getMessage(), $e->getTraceAsString());
                    $this->backend->storeTaskResult($msg, 'error', ['error' => $e->getMessage()], $e->getTraceAsString());
                }
                $this->eventDispatcher->dispatch(new TaskCompletedCeleryEvent($msg), TaskCompletedCeleryEvent::NAME);

                $this->activeTasks += 1;
                $this->activeTasks -= 1;
                $this->processedTasks++;
                return true;
            },
            function () {
                $this->sendHeartbeat();
            }
        );

    }

    private function createEventMessage(array $body = [], array $headers = [], array $properties = [])
    {
        $body       = array_merge($body, [
            'clock'     => $this->logicalClock++,
            'hostname'  => $this->name,
            'timestamp' => microtime(true),
            'utcoffset' => date('Z') / 3600,
            'pid'       => getmypid(),
        ]);
        $headers    = array_merge($headers, [
            'hostname' => $this->name,
        ]);
        $properties = array_merge($properties, []);
        return new CeleryMessage($body, $headers, $properties);
    }

    private function sendTaskReceived(string $taskName, string $taskId, $args = null, $kwArgs = null)
    {
        $this->broker->publish($this->createEventMessage(
            ['type' => 'task-received', 'uuid' => $taskId, 'name' => $taskName, 'args' => $args, 'kwargs' => $kwArgs],
            ['argsrepr' => json_encode($args), 'kwargsrepr' => json_encode($kwArgs)]
        ), 'celeryev');
    }

    private function sendTaskStarted(string $taskId)
    {
        $this->broker->publish($this->createEventMessage(['type' => 'task-started', 'uuid' => $taskId]), 'celeryev');
    }

    private function sendTaskSucceeded(string $taskId, float $runtime, $result)
    {
        $this->broker->publish($this->createEventMessage(['type' => 'task-succeeded', 'uuid' => $taskId, 'runtime' => $runtime, 'result' => $result]), 'celeryev');
    }

    private function sendTaskFailed(string $taskId, string $exception = null, string $traceback = null)
    {
        $this->broker->publish($this->createEventMessage(['type' => 'task-failed', 'uuid' => $taskId, 'exception' => $exception, 'traceback' => $traceback]), 'celeryev');
    }

    private function sendHeartbeat()
    {
        $this->broker->publish($this->createEventMessage(['type' => 'worker-heartbeat', 'active' => $this->activeTasks, 'processed' => $this->processedTasks]), 'celeryev');
    }

    public function registerTask(string $task, string $queue, $fn)
    {
        $this->broker->addQueue($queue);
        $this->handlers[$task] = $fn;
    }

    public function stopAndDisconnect()
    {
        $this->broker->publish($this->createEventMessage(['type' => 'worker-offline']), 'celeryev');

        $this->broker->disconnect();
        $this->backend->disconnect();
    }

}