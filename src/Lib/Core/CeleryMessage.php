<?php


namespace Sungazer\Bundle\SungazerCeleryBundle\Lib\Core;


class CeleryMessage
{
    public const PROP_CONTENT_ENCODING = 'content_encoding';
    public const PROP_CONTENT_TYPE     = 'content_type';

    protected $body;
    protected $headers;
    protected $properties;

    public function __construct(array $body = [], array $headers = [], array $properties = [])
    {
        $this->body       = $body;
        $this->headers    = $headers;
        $this->properties = $properties;
    }

    /**
     * @return array
     */
    public function getBody(): array
    {
        return $this->body;
    }

    /**
     * @param array $body
     * @return CeleryMessage
     */
    public function setBody(array $body): CeleryMessage
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     * @return CeleryMessage
     */
    public function setHeaders(array $headers): CeleryMessage
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @return array
     */
    public function getProperties(): array
    {
        return $this->properties;
    }

    /**
     * @param array $properties
     * @return CeleryMessage
     */
    public function setProperties(array $properties): CeleryMessage
    {
        $this->properties = $properties;
        return $this;
    }

    public function setProperty(string $prop, $value)
    {
        $this->properties[$prop] = $value;
        return $this;
    }

    public function updateProperties(array $properties)
    {
        $this->properties = array_merge($this->properties, $properties);
        return $this;
    }

    public function getProperty(string $prop)
    {
        return $this->properties[$prop] ?? null;
    }

    public function getHeader(string $prop)
    {
        return $this->headers[$prop] ?? null;
    }


}