<?php


namespace Sungazer\Bundle\SungazerCeleryBundle\Lib\Core;


interface CeleryClientInterface
{
    public function connect();
    /**
     * Clears any cache or storage that the storage backend might need.
     * This should be called after each request in case of long lived scripts.
     * WARNING: After calling this, any un-awaited AsyncResult that was created prior to this call will go in timeout!
     */
    public function clear();
    /**
     * @param string $taskName
     * @param string $queue
     * @param array $body = [
     *     'args' => [],
     *     'kwargs' => []
     * ]
     */
    public function dispatchTask(string $taskName, string $queue, array $body): AsyncResult;

    public function setName(string $name);

}