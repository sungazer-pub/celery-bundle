<?php


namespace Sungazer\Bundle\SungazerCeleryBundle\EventSubscriber;


use Sungazer\Bundle\SungazerCeleryBundle\Lib\Core\CeleryClientInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpKernel\Event\TerminateEvent;

class ClearCeleryClientEventSubscriber implements EventSubscriberInterface
{
    /**
     * @var CeleryClientInterface
     */
    private $celeryClient;

    public function __construct(CeleryClientInterface $celeryClient)
    {
        $this->celeryClient = $celeryClient;
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.terminate' => ['clearCeleryCache', -9999]
        ];
    }

    public function clearCeleryCache(TerminateEvent $event)
    {
        $this->celeryClient->clear();
    }
}